Characters
==========

This table shows the encoding used in the info.json files in the first row, and user made encoding in second row.

+-------------------------------------+-------------------------------+-----------------------------+
| Image                               | Letter (Breadcrumbs encoding) | Letter (User-made encoding) |
+=====================================+===============================+=============================+
| .. image:: characters/rule_n.png    | :code:`n`                     | (A)ugustus                  |
+-------------------------------------+-------------------------------+-----------------------------+
| .. image:: characters/rule_t.png    | :code:`t`                     | (T)iberius                  |
+-------------------------------------+-------------------------------+-----------------------------+
| .. image:: characters/rule_q.png    | :code:`q`                     | (G)auis                     |
+-------------------------------------+-------------------------------+-----------------------------+
| .. image:: characters/rule_l.png    | :code:`l`                     | (C)laudis                   |
+-------------------------------------+-------------------------------+-----------------------------+
| .. image:: characters/rule_m.png    | :code:`m`                     | (N)ero                      |
+-------------------------------------+-------------------------------+-----------------------------+
| .. image:: characters/rule_f.png    | :code:`f`                     | (B)ob                       |
+-------------------------------------+-------------------------------+-----------------------------+
| .. image:: characters/rule_w.png    | :code:`w`                     | (O)tho                      |
+-------------------------------------+-------------------------------+-----------------------------+
| .. image:: characters/rule_e.png    | :code:`e`                     | (V)itellius                 |
+-------------------------------------+-------------------------------+-----------------------------+
| .. image:: characters/__FONT1_S.png | :code:`+`                     | (D)omitian                  |
+-------------------------------------+-------------------------------+-----------------------------+
| .. image:: characters/__FONT2_S.png | :code:`.`                     | (H)adrian                   |
+-------------------------------------+-------------------------------+-----------------------------+

Encoding table:

.. image:: characters/Bread_ALPHABET_UPDATE.png
