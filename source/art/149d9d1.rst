149d9d1
=======

https://doughbyte.com/art/?show=149d9d15d9baf4828c52d257b66040aaae1f3adda9ac4507b10580177e5b32ab

given for free 2021-01-28

.. image:: 149d9d1_embedded.png

Hexagon colours: :code:`#5F4341`, :code:`#54414C`, :code:`#595354`

Interpret each byte as ASCII: :code:`_CATALYST`

Sha2 it: :ref:`225f935`
